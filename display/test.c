#include "display.h"

int main(void)
{
	oled disp = {
		.i2c_addr = 0x3C,
		.dev_file = "/dev/i2c-1",
	};

	uint8_t blocks[blocks_count][block_size + 1];

	i2c_init(&disp);

	oled_init(&disp);
	sleep(1);

	uint8_t byte = 0x00;
	uint8_t i, j;
	for (i = 0; i < blocks_count; ++i) {
		byte = i & (1 << 2) ? 0x00 : 0xFF;
		memset(disp.block_table[i] + 1, byte, block_size / 4);
		memset(disp.block_table[i] + block_size / 2 + 1, byte,
		       block_size / 4);
		byte = i & (1 << 2) ? 0xFF : 0x00;
		memset(disp.block_table[i] + block_size / 4 + 1, byte,
		       block_size / 4);
		memset(disp.block_table[i] + 3 * block_size / 4 + 1, byte,
		       block_size / 4);
		disp.new_block[i] = true;
		oled_update(&disp);
		usleep(10000);
	}

	sleep(1);
	i2c_exit(&disp);
}
