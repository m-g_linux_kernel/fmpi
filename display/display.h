#ifndef DISPLAY_H
#define DISPLAY_H
#include <errno.h>
#include <fcntl.h>
#include <linux/i2c-dev.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <sys/ioctl.h>
#include <unistd.h>

#define MAX_PATH_LEN 20
#define BLOCK_SIZE 32
#define BLOCKS_COUNT 32

static const uint8_t width = 128;
static const uint8_t height = 64;
static const uint8_t block_size = 32;
static const uint8_t blocks_count = 32;

typedef struct {
	int fd;
	uint8_t i2c_addr;
	char dev_file[MAX_PATH_LEN];
	uint8_t block_table[BLOCKS_COUNT][BLOCK_SIZE + 1];
	bool new_block[BLOCKS_COUNT];
} oled;

enum Command {
	//command type
	COMMAND = 0x00,
	DATA = 0x40,
	//init configurations
	CHARGE_PUMP_SETTING = 0x8D,
	ENABLE_CHARGE_PUMP = 0x14,
	SET_MEMORY_ADDRESSING_MODE = 0x20,
	HORIZONTAL_ADDRESSING_MODE = 0x00,
	SET_COLUMN_ADDRESS = 0x21,
	SET_PAGE_ADDRESS = 0x22,
	SET_DISPLAY_START_LINE = 0x40,
	SET_CONTRAST_CONTROL = 0x81,
	SET_SEGMENT_REMAP_127 = 0xA1,
	ENTIRE_DISPLAY_ON_RESUME = 0xA4,
	SET_NORMAL_DISPLAY = 0xA6,
	SET_MUX_RATIO = 0xA8,
	SET_DISPLAY_ON = 0xAF,
	SET_COM_OUTPUT_REMAPED = 0xC8,
	SET_DISPLAY_OFFSET = 0xD3,
	SET_OSC_FREQUENCY = 0xD5,
	SET_PRECHARGE_PERIOD = 0xD9,
	SET_COM_PINS_HARDWARE_CONFIGURATION = 0xDA,
	SET_VCOMH_DESELECT_LEVEL = 0xDB,
	//block write position
	SET_PAGE_START = 0xB0,
	SET_LOWER_COLUMN_START = 0x00,
	SET_HIGHER_COLUMN_START = 0x10,
};

int i2c_init(oled *dev);
void oled_init(oled *dev);
int i2c_exit(oled *dev);
void oled_exit(oled *dev);
void send_command(oled *dev, const uint8_t command);
void send_double_command(oled *dev, const uint8_t command, const uint8_t arg);
void send_long_command(oled *dev, const uint8_t command, const uint8_t arg1,
		       const uint8_t arg2);
void send_block_data(oled *dev, uint8_t *data, const uint8_t page,
		     const uint8_t column);
void oled_update(oled *dev);
void set_update_flags(
	oled *dev,
	bool flag); //true - full next display update; false - ignore next display update
void set_blocks_state(
	oled *dev, uint8_t byte); //0x00 - all pixels off; 0xFF - all pixels on
#endif
