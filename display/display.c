#include "display.h"

int i2c_init(oled *dev)
{
	dev->fd = open(dev->dev_file, O_RDWR);
	if (dev->fd < 0) {
		printf("Error opening file: %s\n", strerror(errno));
		return 1;
	}
	if (ioctl(dev->fd, I2C_SLAVE, dev->i2c_addr) < 0) {
		printf("ioctl error: %s\n", strerror(errno));
		return 1;
	}
	return 0;
}

int i2c_exit(oled *dev)
{
	if (close(dev->fd) == -1) {
		printf("close error: %s\n", strerror(errno));
		return 1;
	}
	return 0;
}

void send_command(oled *dev, const uint8_t command)
{
	uint8_t data[2] = { COMMAND, command };
	if (write(dev->fd, data, 2) < 0) {
		printf("Error writing file: %s\n", strerror(errno));
	}
}

void send_double_command(oled *dev, const uint8_t command, const uint8_t arg)
{
	uint8_t data[] = { COMMAND, command, arg };
	if (write(dev->fd, data, 3) < 0) {
		printf("Error writing file: %s\n", strerror(errno));
	}
}

void send_long_command(oled *dev, const uint8_t command, const uint8_t arg1,
		       const uint8_t arg2)
{
	uint8_t data[] = { COMMAND, command, arg1, arg2 };
	if (write(dev->fd, data, 4) < 0) {
		printf("Error writing file: %s\n", strerror(errno));
	}
}

void send_block_data(oled *dev, uint8_t *data, const uint8_t page,
		     const uint8_t column)
{
	data[0] = DATA;
	send_command(dev, SET_PAGE_START | page);
	send_command(dev, SET_LOWER_COLUMN_START | column & 0x0F);
	send_command(dev, SET_HIGHER_COLUMN_START | (column >> 4) & 0x0F);
	if (write(dev->fd, data, block_size + 1) < 0) {
		printf("Error writing file: %s\n", strerror(errno));
	}
}

void oled_update(oled *dev)
{
	uint8_t page = 0;
	uint8_t column = 0;
	uint8_t i = 0;
	for (; i < blocks_count; ++i) {
		if (dev->new_block[i]) {
			send_block_data(dev, dev->block_table[i], page, column);
			dev->new_block[i] = false;
		}
		column += block_size;
		if (column >= width) {
			column = 0;
			++page;
		}
	}
}

void set_update_flags(oled *dev, bool flag)
{
	memset(dev->new_block, flag, blocks_count);
}

void set_blocks_state(oled *dev, uint8_t byte)
{
	memset(dev->block_table, byte, blocks_count * (block_size + 1));
	set_update_flags(dev, true);
}

void oled_init(oled *dev)
{
	send_double_command(dev, SET_MUX_RATIO, 0x3F);

	send_double_command(dev, SET_DISPLAY_OFFSET, 0x00);

	send_command(dev, SET_DISPLAY_START_LINE);

	send_command(dev, SET_SEGMENT_REMAP_127);

	send_command(dev, SET_COM_OUTPUT_REMAPED);

	send_double_command(dev, SET_COM_PINS_HARDWARE_CONFIGURATION, 0x12);

	send_double_command(dev, SET_PRECHARGE_PERIOD, 0xF1);

	send_double_command(dev, SET_VCOMH_DESELECT_LEVEL, 0x40);

	send_double_command(dev, SET_CONTRAST_CONTROL, 0x7F);

	send_command(dev, ENTIRE_DISPLAY_ON_RESUME);

	send_command(dev, SET_NORMAL_DISPLAY);

	send_double_command(dev, SET_MEMORY_ADDRESSING_MODE,
			    HORIZONTAL_ADDRESSING_MODE);

	send_long_command(dev, SET_COLUMN_ADDRESS, 0x00, 0x7F);

	send_long_command(dev, SET_PAGE_ADDRESS, 0x00, 0x07);

	send_double_command(dev, SET_OSC_FREQUENCY, 0x80);

	send_double_command(dev, CHARGE_PUMP_SETTING, ENABLE_CHARGE_PUMP);

	set_blocks_state(dev, 0x00);

	oled_update(dev);

	send_command(dev, SET_DISPLAY_ON);
}
