#include <stdio.h>
#include <sys/time.h>

#include "encoder.h"

#define GPIO_SW 17
#define GPIO_CLK 27
#define GPIO_DT 22
#define POLL_TIMEOUT 2000

int main(void)
{
	int rc = 0;
	struct timeval s_time = { 0, 0 };
	struct timeval f_time = { 0, 0 };
	long msec = 0;
	encoder enc = {
		.gpio_sw = GPIO_SW,
		.gpio_clk = GPIO_CLK,
		.gpio_dt = GPIO_DT,
	};

	encoder_init(&enc);

	while (1) {
		rc = gpio_poll(&enc, POLL_TIMEOUT);
		printf("rc = %d\n", rc);
		if (rc < 0)
			printf("Error\n");

		if (rc > 0)
			switch (enc.state) {
			case REST_ST:
				if (s_time.tv_usec) {
					gettimeofday(&f_time, NULL);
					msec = (f_time.tv_sec - s_time.tv_sec) *
						       1000 +
					       (f_time.tv_usec -
						s_time.tv_usec) /
						       1000;
					printf("Button up\ttime = %d ms\n",
					       msec);
					s_time.tv_usec = 0;
				}
				break;
			case RIGHT_ST:
				printf("Turn right\n");
				break;
			case LEFT_ST:
				printf("Turn left\n");
				break;
			case PUSH_ST:
				if (s_time.tv_usec == 0)
					gettimeofday(&s_time, NULL);
				printf("Button down\n");
				break;
			}
	}

	encoder_exit(&enc);
	return 0;
}
