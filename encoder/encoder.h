#ifndef ENCODER_H
#define ENCODER_H
#include <fcntl.h>
#include <poll.h>
#include <stdio.h>
#include <string.h>
#include <sys/time.h>
#include <unistd.h>

#define SYSFS_GPIO_DIR "/sys/class/gpio"
#define MAX_BUF 128

enum State { REST_ST = 0, RIGHT_ST = 1, LEFT_ST = 2, PUSH_ST = 3 };

typedef struct {
	unsigned int gpio_clk;
	unsigned int gpio_dt;
	unsigned int gpio_sw;
	struct pollfd fdset[3];
	int state;
} encoder;

int gpio_export(unsigned int gpio);
int gpio_unexport(unsigned int gpio);
int gpio_set_dir_in(unsigned int gpio);
int gpio_get_value(unsigned int gpio);
int gpio_set_edge(unsigned int gpio, const char *edge);
int gpio_fd_open(unsigned int gpio);
int gpio_fd_close(int fd);
int gpio_poll(encoder *devp, int timeout);
int encoder_init(encoder *devp);
int encoder_exit(const encoder *devp);

#define START_CLOCK(start)                          \
	{                                           \
		if (start.tv_usec == 0)             \
			gettimeofday(&start, NULL); \
	}

#define TERM_CLOCK(start, finish, msec)                          \
	{                                                        \
		gettimeofday(&f_time, NULL);                     \
		msec = (f_time.tv_sec - s_time.tv_sec) * 1000 +  \
		       (f_time.tv_usec - s_time.tv_usec) / 1000; \
	}

#endif
