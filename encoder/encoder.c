#include "encoder.h"

static char buffer[MAX_BUF];

int encoder_init(encoder *devp)
{
	gpio_export(devp->gpio_sw);
	gpio_export(devp->gpio_clk);
	gpio_export(devp->gpio_dt);
	gpio_set_dir_in(devp->gpio_sw);
	gpio_set_dir_in(devp->gpio_clk);
	gpio_set_dir_in(devp->gpio_dt);
	gpio_set_edge(devp->gpio_sw, "both");
	gpio_set_edge(devp->gpio_clk, "both");
	gpio_set_edge(devp->gpio_dt, "both");
	devp->state = 0;
	memset((void *)devp->fdset, 0, sizeof(devp->fdset));
	devp->fdset[0].fd = gpio_fd_open(devp->gpio_sw);
	devp->fdset[1].fd = gpio_fd_open(devp->gpio_clk);
	devp->fdset[2].fd = gpio_fd_open(devp->gpio_dt);
	int i = 0;
	for (; i < 3; ++i)
		devp->fdset[i].events = POLLPRI;
	int rc = 0;
	do {
		rc = poll(devp->fdset, 3, 100);
		for (i = 0; i < 3; ++i)
			read(devp->fdset[i].fd, buffer, MAX_BUF);
	} while (rc != 0);
}

int encoder_exit(const encoder *devp)
{
	gpio_unexport(devp->gpio_sw);
	gpio_unexport(devp->gpio_clk);
	gpio_unexport(devp->gpio_dt);
}

int gpio_export(const unsigned int gpio)
{
	int fd;
	int len;
	fd = open(SYSFS_GPIO_DIR "/export", O_WRONLY);
	if (fd < 0)
		return fd;
	len = snprintf(buffer, sizeof(buffer), "%d", gpio);
	write(fd, buffer, len);
	close(fd);
	return 0;
}

int gpio_unexport(const unsigned int gpio)
{
	int fd;
	int len;
	fd = open(SYSFS_GPIO_DIR "/unexport", O_WRONLY);
	if (fd < 0)
		return fd;
	len = snprintf(buffer, sizeof(buffer), "%d", gpio);
	write(fd, buffer, len);
	close(fd);
	return 0;
}

int gpio_set_dir_in(const unsigned int gpio)
{
	int fd;
	snprintf(buffer, sizeof(buffer), SYSFS_GPIO_DIR "/gpio%d/direction",
		 gpio);
	fd = open(buffer, O_WRONLY);
	if (fd < 0) {
		perror("gpio/direction");
		return fd;
	}
	write(fd, "in", 3);
	close(fd);
	return 0;
}

int gpio_get_value(const unsigned int gpio)
{
	int fd;
	char ch;
	int value;
	snprintf(buffer, sizeof(buffer), SYSFS_GPIO_DIR "/gpio%d/value", gpio);
	fd = open(buffer, O_RDONLY);
	if (fd < 0) {
		perror("gpio/get-value");
		return fd;
	}
	read(fd, &ch, 1);
	value = ch != '0' ? 1 : 0;
	close(fd);
	return value;
}

int gpio_set_edge(const unsigned int gpio, const char *edge)
{
	int fd;
	snprintf(buffer, sizeof(buffer), SYSFS_GPIO_DIR "/gpio%d/edge", gpio);
	fd = open(buffer, O_WRONLY);
	if (fd < 0) {
		perror("gpio/set-edge");
		return fd;
	}
	write(fd, edge, strlen(edge) + 1);
	close(fd);
	return 0;
}

int gpio_fd_open(const unsigned int gpio)
{
	int fd;
	snprintf(buffer, sizeof(buffer), SYSFS_GPIO_DIR "/gpio%d/value", gpio);
	fd = open(buffer, O_RDONLY | O_NONBLOCK);
	if (fd < 0) {
		perror("gpio/fd_open");
	}
	return fd;
}

#define POLL_ST_UPDATE(state)                                \
	{                                                    \
		if (state == RIGHT_ST || state == LEFT_ST) { \
			state = REST_ST;                     \
		}                                            \
	}

#define POLL_CLK_CHECK(fdset_i, turn_st)                   \
	{                                                  \
		if (fdset_i.revents & POLLPRI) {           \
			read(fdset_i.fd, buffer, MAX_BUF); \
			if (turn_st & 1)                   \
				--turn_st;                 \
			else                               \
				++turn_st;                 \
		}                                          \
	}

#define POLL_DT_CHECK(fdset_i, turn_st)                    \
	{                                                  \
		if (fdset_i.revents & POLLPRI) {           \
			read(fdset_i.fd, buffer, MAX_BUF); \
			if (turn_st & 1)                   \
				++turn_st;                 \
			else                               \
				--turn_st;                 \
		}                                          \
	}

#define POLL_SW_CHECK(fdset_i, state)                      \
	{                                                  \
		if (fdset_i.revents & POLLPRI) {           \
			read(fdset_i.fd, buffer, MAX_BUF); \
			if (state != PUSH_ST)              \
				state = PUSH_ST;           \
			else                               \
				state = REST_ST;           \
			return 1;                          \
		}                                          \
	}

#define POLL_TURN_CHECK(state, turn_st)                                  \
	{                                                                \
		if (state == REST_ST && !(turn_st & 2 || turn_st & 1)) { \
			if (turn_st > 0)                                 \
				state = RIGHT_ST;                        \
			else if (turn_st < 0)                            \
				state = LEFT_ST;                         \
			turn_st = 0;                                     \
			return 1;                                        \
		}                                                        \
	}

int gpio_poll(encoder *devp, const int timeout)
{
	static const int nfds = 3;
	static int turn_st = REST_ST;

	poll(devp->fdset, nfds, timeout);

	POLL_CLK_CHECK(devp->fdset[1], turn_st);

	POLL_DT_CHECK(devp->fdset[2], turn_st);

	POLL_SW_CHECK(devp->fdset[0], devp->state);

	POLL_TURN_CHECK(devp->state, turn_st);

	return 0;
}
