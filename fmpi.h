#ifndef FMPI_H
#define FMPI_H

#include "display/display.h"
#include "encoder/encoder.h"
#include "fm/fm.h"
#include "fparser/fparser.h"
#include "render/render.h"

#include <pthread.h>

enum gpio {
	sw = 17,
	clk = 22,
	dt = 27,
	i2c_addr = 0x3c,
};

#define DEV_FILE "/dev/i2c-1"

enum time { poll_timeout = 3000, sh_ph = 500, md_ph = 2000 };

const uint8_t visible_count = 4;

void *poll_thread(void *arg);

#endif
