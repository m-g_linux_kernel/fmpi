SOURCES=fmpi.c encoder/encoder.c display/display.c render/render.c fparser/fparser.c fm/fm.c
OBJECTS=$(SOURCES:.c=.o)
EXECUTABLE=fmpi
PACKAGE=fmpi

.PHONY: clean all install

all: $(EXECUTABLE)

$(EXECUTABLE): $(OBJECTS)
	mkdir bin
	gcc -o $(EXECUTABLE) -lpthread $(OBJECTS)

bin/%.o: %.c
	gcc -c $< -o $@

install: all
	sudo cp bin/$(EXECUTABLE) /usr/bin
	sudo ./install.sh

uninstall:
	sudo rm /usr/bin/$(EXECUTABLE)

clear:
	rm -rf bin
	rm -rf *.deb $(PACKAGE)
