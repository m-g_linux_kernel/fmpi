#include "fparser.h"

//#define DEBUG

#ifdef DEBUG
#define DISPLAY_WIDTH 15
#endif

void test(const char* command)
{
        // Write/rewrite "ls" output in file
        system(command);
        
        // Initializing table of files and directories
        init_table("temp.txt");
        size_t sz = get_table_size();
        for ( size_t i = 0; i < sz; ++i ) {
#ifndef DEBUG
                printf("%s\n", get_table_string(i));
#else
                const char* str = get_table_string(i);
                for ( size_t j = 0; j < DISPLAY_WIDTH; ++j ) {
                        char c = str[j];
                        if ( c == '\0' )
                                printf("!");
                        else
                                printf("%c", c);
                }
                printf("\n");
#endif
        }
        printf("\n");
        // Deleting allocated memory
        release_table();
        
        // Get what is in file and delete it
        system("cat temp.txt; rm temp.txt");
}

int main()
{
        test("ls temp > temp.txt");
        printf("\n\n");
        test("ls > temp.txt");
        printf("\n\n");
        test("ls temp > temp.txt");

        return 0;
}
