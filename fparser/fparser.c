#include "fparser.h"

#define DISPLAY_WIDTH 15
static FILE* fp = (FILE*)0;
static size_t file_size = 0u;
static size_t table_size = 0u;
static char** table = (char**)0;
#define TRUE 1
#define FALSE 0
static int flag = FALSE;

// Check if table is initialized
int is_table_allocated(void)
{
        return flag;
}

// Return current size of table
size_t get_table_size(void)
{
        return table_size;
}

/* Get string on position "i" in the table or 
 * return something else in exceptional cases */
const char* get_table_string(size_t i)
{
        if ( !table_size )
                return "empty";
        else if ( i < table_size )
                return table[i];
        else
                return "";
}

// Just open file
#define OPEN_FILE(path)        \
{                              \
        fp = fopen(path, "r"); \
        if ( !fp )             \
                return;        \
}

/* Allocate the buffer and
 * fill the table with
 * the contents of the file */
#define MAKE_BUFFER(buff)                              \
{                                                      \
        fseek(fp, 0, SEEK_END);                        \
        if ( !(file_size = ftell(fp)) )                \
                return;                                \
        rewind(fp);                                    \
        buff = (char*)malloc(sizeof(char) * file_size);\
        fread(buff, 1, file_size, fp);                 \
}

// Count size of strings in the table
#define COUNT_TABLE_SIZE(buff)                     \
{                                                  \
        for ( size_t i = 0; i < file_size; ++i ) { \
                if ( buff[i] == '\n' )             \
                        ++table_size;              \
        }                                          \
        if ( !table_size )                         \
                return;                            \
}

/* Allocate the table with the already known table_size
 * and fill strings of the table with '\0' */
#define ALLOC_TABLE()                                  \
{                                                      \
        table = (char**)malloc(                        \
                sizeof(char*) * table_size);           \
        for ( size_t i = 0; i < table_size; ++i ) {    \
                *(table + i) = (char*)malloc(          \
                        sizeof(char) * DISPLAY_WIDTH); \
                size_t j = 0;                          \
                while ( j < DISPLAY_WIDTH ) {          \
                        *(*(table + i) + j++) = '\0';  \
                }                                      \
        }                                              \
}

/* Fill the table with characters
 * from the buffer without '\n' */
#define FILL_TABLE(buff)                            \
{                                                   \
        size_t col = 0;                             \
        size_t cnt = 0;                             \
        for ( size_t i = 0; i < file_size; ++i ) {  \
                if ( *(buffer + i) == '\n' ) {      \
                        cnt = 0;                    \
                        ++col;                      \
                        continue;                   \
                }                                   \
                if ( cnt != DISPLAY_WIDTH )         \
                        *(*(table + col) + cnt++) = \
                                *(buffer + i);      \
        }                                           \
}

/* Fill the file named "path" with strings
 * recieved from the command "ls" and
 * initializing table with strings from the file */
void init_table(const char* path)
{
        // Check allocating of the table
        // If the table is reused - free memory
        if ( flag )
                release_table();

        char* buffer;

        OPEN_FILE(path);

        MAKE_BUFFER(buffer);

        COUNT_TABLE_SIZE(buffer);

        ALLOC_TABLE();

        flag = TRUE;

        FILL_TABLE(buffer);

        free(buffer);
        fclose(fp);
}

// Freeing allocated memory for the table
void release_table(void)
{
        if ( !flag )
            return;

        for ( size_t i = 0; i < table_size; ++i ) {
                free(*(table + i));
        }
        free(table);
        file_size = 0;
        table_size = 0;
        flag = FALSE;
}
