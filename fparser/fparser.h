#pragma once

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int is_table_allocated(void);
size_t get_table_size(void);
const char* get_table_string(size_t);
void init_table(const char*);
void release_table(void);
