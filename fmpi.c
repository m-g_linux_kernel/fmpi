#include "fmpi.h"

encoder enc = {
	.gpio_sw = sw,
	.gpio_clk = clk,
	.gpio_dt = dt,
};
encoder *encp = &enc;

int main(void)
{
	int rc = 0;
	long msec = 0;
	bool exit_flag = false;
	bool redraw = false;
	bool full_redraw = false;
	struct timeval s_time = { 0, 0 };
	struct timeval f_time = { 0, 0 };

	oled disp = {
		.i2c_addr = i2c_addr,
		.dev_file = DEV_FILE,
	};
	oled *dispp = &disp;

	fmng fm;
	fmng *fmp = &fm;

	pthread_t poll_tid;

	encoder_init(encp);
	i2c_init(dispp);
	oled_init(dispp);
	fmng_init(fmp, "/");
	pthread_create(&poll_tid, NULL, poll_thread, NULL);

	show_dirs(dispp, fm.current_dir, fm.start_pos);
	show_select(dispp, fm.current_pos, '>');
	show_pos_number(dispp, fm.start_pos + fm.current_pos + 1,
			fm.table_size);
	oled_update(dispp);

	while (!exit_flag) {
		switch (enc.state) {
		case REST_ST:
			if (s_time.tv_usec) {
				TERM_CLOCK(s_time, f_time, msec);
				s_time.tv_usec = 0;
				if (msec < sh_ph && cd_up(fmp)) {
					full_redraw = true;
					break;
				} else if (msec < md_ph && cd_down(fmp)) {
					full_redraw = true;
					break;
				} else {
					exit_flag = true;
				}
			}
			break;
		case RIGHT_ST:
			if (inc_pos(fmp)) {
				full_redraw = true;
			} else {
				redraw = true;
			}
			enc.state = REST_ST;
			break;
		case LEFT_ST:
			if (decr_pos(fmp)) {
				full_redraw = true;
			} else {
				redraw = true;
			}
			enc.state = REST_ST;
			break;
		case PUSH_ST:
			START_CLOCK(s_time);
			break;
		}
		if (full_redraw) {
			show_dirs(dispp, fm.current_dir, fm.start_pos);
		}
		if (full_redraw || redraw) {
			show_select(dispp, fm.current_pos, '>');
			show_pos_number(dispp,
					fm.start_pos + fm.current_pos + 1,
					fm.table_size);
			oled_update(dispp);
		}
	}

	pthread_cancel(poll_tid);
	encoder_exit(encp);
	i2c_exit(dispp);
	fmng_exit(fmp);

	return 0;
}

void *poll_thread(void *arg)
{
	while (1) {
		gpio_poll(encp, poll_timeout);
	}
	return NULL;
}
