#ifndef FM_H
#define FM_H

#include "../fparser/fparser.h"
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>

#define PATH_STR_SIZE 64
#define BUFF_SIZE 128
#define BIN_DIR "bin"
#define OUTPUT_FILE "bin/lsout.txt"
#define REDRAW 1
#define END_OF_TABLE 2

extern const uint8_t visible_count;

typedef struct {
	char current_dir[PATH_STR_SIZE];
	size_t table_size;
	size_t start_pos;
	uint8_t current_pos;
} fmng;

void fmng_init(fmng *fm, const char *path);
void fmng_exit(fmng *fm);
int inc_pos(fmng *fm);
int decr_pos(fmng *fm);
int is_dir(fmng *fm);
int cd_up(fmng *fm);
int cd_down(fmng *fm);

#endif
