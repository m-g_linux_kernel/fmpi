#include "fm.h"

const uint8_t visible_count = 5;

int main(void)
{
	fmng fm;
	fmng *fmp = &fm;
	fmng_init(fmp, "/home/pi/fmpi");

	int cd_count = 10;
	printf("%d change dir actions:\n", cd_count);
	int i = 0;
	for (i = 0; i < cd_count; ++i) {
		int j = 0;
		for (j = 0; j < visible_count; ++j) {
			int item_flag = is_dir(fmp);
			if (j == fm.current_pos)
				printf("*%s %d\n",
				       get_table_string(fm.start_pos + j),
				       item_flag);
			else
				printf("%s\n",
				       get_table_string(fm.start_pos + j));
		}
		printf("\n");
		if (i < 5 && cd_up(fmp))
			printf("pwd: %s\n", fm.current_dir);
		else if (i > 4 && cd_down(fmp))
			printf("pwd: %s\n", fm.current_dir);
	}

	fmng_exit(fmp);
	return 0;
}
