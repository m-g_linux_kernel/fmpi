#include "fm.h"

static char buffer[BUFF_SIZE] = { 0 };

void fmng_init(fmng *fm, const char *path)
{
	memset(fm->current_dir, 0, PATH_STR_SIZE);
	strncpy(fm->current_dir, path, PATH_STR_SIZE);
	snprintf(buffer, BUFF_SIZE, "ls %s > %s", fm->current_dir, OUTPUT_FILE);
	system(buffer);
	init_table(OUTPUT_FILE);
	fm->table_size = get_table_size();
	fm->start_pos = 0;
	fm->current_pos = 0;
}

void fmng_exit(fmng *fm)
{
	release_table();
}

int inc_pos(fmng *fm)
{
	if (++fm->current_pos + fm->start_pos == fm->table_size) {
		fm->start_pos = 0;
		fm->current_pos = 0;
		return REDRAW | END_OF_TABLE;
	}
	if (fm->current_pos == visible_count) {
		fm->start_pos += visible_count;
		fm->current_pos = 0;
		return REDRAW;
	}
	return 0;
}

int decr_pos(fmng *fm)
{
	if (fm->current_pos + fm->start_pos == 0) {
		fm->current_pos = fm->table_size % visible_count - 1;
		fm->start_pos = fm->table_size - fm->current_pos - 1;
		return REDRAW | END_OF_TABLE;
	}
	if (fm->current_pos == 0) {
		fm->current_pos = visible_count - 1;
		fm->start_pos -= visible_count;
		return REDRAW;
	}
	--fm->current_pos;
	return 0;
}

int is_dir(fmng *fm)
{
	static struct stat st;
	int res = 0;
	const char *current_item =
		is_table_allocated() ?
			get_table_string(fm->start_pos + fm->current_pos) :
			NULL;
	if (current_item && strlen(current_item))
		res = snprintf(buffer, BUFF_SIZE, "%s%s", fm->current_dir,
			       current_item);
	if (res) {
		stat(buffer, &st);
		return S_ISDIR(st.st_mode);
	}
	return res;
}

int cd_up(fmng *fm)
{
	if (is_table_allocated() && is_dir(fm)) {
		strncpy(fm->current_dir, buffer, PATH_STR_SIZE);
		strncat(fm->current_dir, "/", PATH_STR_SIZE);
		snprintf(buffer, BUFF_SIZE, "ls %s > %s", fm->current_dir,
			 OUTPUT_FILE);
		system(buffer);
		init_table(OUTPUT_FILE);
		fm->table_size = get_table_size();
		fm->start_pos = 0;
		fm->current_pos = 0;
		return true;
	}
	return false;
}

int cd_down(fmng *fm)
{
	int it = strlen(fm->current_dir);
	if (it-- < 2)
		return false;
	fm->current_dir[it] = '\0';
	for (; it + 1; --it) {
		if (fm->current_dir[it] != '/') {
			fm->current_dir[it] = '\0';
		} else {
			snprintf(buffer, BUFF_SIZE, "ls %s > %s",
				 fm->current_dir, OUTPUT_FILE);
			system(buffer);
			init_table(OUTPUT_FILE);
			fm->table_size = get_table_size();
			fm->start_pos = 0;
			fm->current_pos = 0;
			return true;
		}
	}
	return true;
}
