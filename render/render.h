#ifndef RENDER_H
#define RENDER_H

#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>

#include "../display/display.h"
#include "../fparser/fparser.h"
#include "font6x8.h"

#define NM_POS 8

enum Offset {
	PWD_X = 2,
	PWD_Y = 2,
	SLCT_X = 4,
	DIR_X = 12,
	DIR_Y = 14,
	LN_SPACE = 10,
	POSN_X = 80,
	POSN_Y = 54
};

extern const uint8_t visible_count;

void set_pixel(oled *dev, uint8_t x, uint8_t y);
void set_byte(oled *dev, uint8_t x, uint8_t y, uint8_t byte);
void set_byte(oled *dev, uint8_t x, uint8_t y, uint8_t byte);
void put_char(oled *dev, uint8_t x, uint8_t y, char ch);
void put_string(oled *dev, uint8_t x, uint8_t y, const char *str);
void show_dirs(oled *dev, const char *pwd, size_t start_pos);
void show_select(oled *dev, uint8_t select_pos, char select_sym);
void show_pos_number(oled *dev, uint8_t current_pos, uint8_t table_size);

#endif
