#include "render.h"

void set_pixel(oled *dev, uint8_t x, uint8_t y)
{
	if (x >= width)
		x = width - 1;
	if (y >= height)
		y = height - 1;
	uint8_t block_x = x >> 5;
	uint8_t block_y = y >> 3;
	uint8_t block_it = (block_y << 2) + block_x;
	uint8_t byte_it = x - (block_x << 5);
	uint8_t bit_it = y - (block_y << 3);
	dev->block_table[block_it][byte_it + 1] |= 1 << bit_it;
	dev->new_block[block_it] = true;
}

void set_byte(oled *dev, uint8_t x, uint8_t y, uint8_t byte)
{
	if (x >= width)
		x = width - 1;
	if (y >= height)
		y = height - 1;
	uint8_t block_x = x >> 5;
	uint8_t block_y = y >> 3;
	uint8_t block_it = (block_y << 2) + block_x;
	uint8_t byte_it = x - (block_x << 5);
	uint8_t bit_it = y - (block_y << 3);
	dev->block_table[block_it][byte_it + 1] &= 0xFF >> (8 - bit_it);
	dev->block_table[block_it][byte_it + 1] |= byte << bit_it;
	dev->new_block[block_it] = true;
	if (block_y < 7) {
		block_it += 4;
		dev->block_table[block_it][byte_it + 1] &= 0xFF << bit_it;
		dev->block_table[block_it][byte_it + 1] |= byte >> (8 - bit_it);
		dev->new_block[block_it] = true;
	}
}

void put_char(oled *dev, uint8_t x, uint8_t y, char ch)
{
	if (x >= width || y >= height)
		return;
	uint8_t i = 0;
	for (; i < FONT6X8_WIDTH; ++i) {
		if (x + i == width)
			return;
		set_byte(dev, x + i, y, font6x8[((int)ch - 32) * 6 + i]);
	}
}

void put_string(oled *dev, uint8_t x, uint8_t y, const char *str)
{
	if (x >= width || y >= height)
		return;
	uint8_t strln = strlen(str);
	uint8_t i = 0;
	for (; i < strln; ++i) {
		if (x + (i + 1) * 6 >= width)
			return;
		put_char(dev, x + i * 6, y, *(str + i));
	}
}

void show_dirs(oled *dev, const char *pwd, size_t start_pos)
{
	uint8_t y_pos = DIR_Y;
	set_blocks_state(dev, 0x00);
	put_string(dev, PWD_X, PWD_Y, pwd);
	for (size_t i = 0; i < visible_count; ++i) {
		put_string(dev, DIR_X, y_pos, get_table_string(start_pos + i));
		y_pos += LN_SPACE;
	}
}

void show_select(oled *dev, uint8_t select_pos, char select_sym)
{
	uint8_t y_pos = DIR_Y;
	for (uint8_t i = 0; i < visible_count; ++i) {
		if (i != select_pos)
			put_char(dev, SLCT_X, y_pos, ' ');
		else
			put_char(dev, SLCT_X, y_pos, '>');
		y_pos += LN_SPACE;
	}
}

void show_pos_number(oled *dev, uint8_t current_pos, uint8_t table_size)
{
	static char buffer[NM_POS];
	snprintf(buffer, NM_POS, "%d/%d", current_pos, table_size);
	put_string(dev, POSN_X, POSN_Y, buffer);
}
