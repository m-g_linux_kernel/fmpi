#include "render.h"

const uint8_t visible_count = 4;

int main(void)
{
	oled disp = {
		.i2c_addr = 0x3c,
		.dev_file = "/dev/i2c-1",
	};
	i2c_init(&disp);
	oled_init(&disp);
	sleep(1);
	send_command(&disp, 0xA7);

	system("ls /sys/class > dev.txt");
	init_table("dev.txt");
	size_t tb_size = get_table_size();
	uint8_t select = 0;
	for (size_t i = 0; i < tb_size; ++i) {
		if (select == 0) {
			show_dirs(&disp, "/", i);
		}
		show_select(&disp, select, '>');
		show_pos_number(&disp, i + 1, tb_size);
		if (++select == visible_count)
			select = 0;
		oled_update(&disp);
		sleep(1);
	}

	release_table();
	system("rm dev.txt");
	i2c_exit(&disp);
	return 0;
}
